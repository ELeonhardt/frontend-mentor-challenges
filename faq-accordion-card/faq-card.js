const buttons = Array.from(document.querySelectorAll('.faq-button'));

function toggleFaqAnswer(event) {
    const currentLi = event.currentTarget.parentElement;
    currentLi.querySelector('.question').classList.toggle('bold');
    currentLi.querySelector('.answer').classList.toggle('hidden');
    currentLi.querySelector('.button-icon').classList.toggle('inverted');
}

buttons.forEach(b => b.addEventListener('click', toggleFaqAnswer));